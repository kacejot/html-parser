#include <fstream>
#include <gtest\gtest.h>
#include "html_parser.h"

using domParser::DocumentObjectType;

TEST(HtmlParserTest, TagNameTest)
{
	domParser::HtmlParser parser;
	auto object = parser.get_dom_object("tag_name_test.html");
	ASSERT_EQ(object->get_children()[0]->get_name(), "TagName");
}

TEST(HtmlParserTest, DoctypeTest)
{
	domParser::HtmlParser parser;
	auto object = parser.get_dom_object("special_tag_name_test.html");
	ASSERT_EQ(object->get_children()[0]->get_name(), "!DOCTYPE");
}

TEST(HtmlParserTest, AttributesTest)
{
	domParser::HtmlParser parser;

	auto object = parser.get_dom_object("attributes_test.html")->get_children()[0];
	auto name = object->get_name();
	auto attrs = object->get_attributes();
	auto first_attr = attrs[0];
	auto second_attr = attrs[1];

	ASSERT_EQ(name, "META");
	ASSERT_EQ(first_attr.name, "NAME");
	ASSERT_EQ(first_attr.value, "\"GENERATOR\"");
	ASSERT_EQ(second_attr.name, "Content");
	ASSERT_EQ(second_attr.value, "\"Microsoft Visual Studio\"");
}

TEST(HtmlParserTest, NullAttributeTest)
{
	domParser::HtmlParser parser;

	auto object = parser.get_dom_object("null_attribute_test.html")->get_children()[0];
	auto name = object->get_name();
	auto attrs = object->get_attributes();
	auto first_attr = attrs[0];
	auto second_attr = attrs[1];

	ASSERT_EQ(name, "META");
	ASSERT_EQ(first_attr.name, "NAME");
	ASSERT_EQ(first_attr.value, "\"GENERATOR\"");
	ASSERT_EQ(second_attr.name, "Content");
	ASSERT_EQ(second_attr.value, "");
}

TEST(HtmlParserTest, ContentTest)
{
	domParser::HtmlParser parser;
	auto root = parser.get_dom_object("content_test.html");

	ASSERT_TRUE(root->get_content().empty());
	ASSERT_TRUE(root->get_attributes().empty());
	ASSERT_EQ(root->get_type(), DocumentObjectType::File);
	ASSERT_EQ(root->get_parent(), nullptr);

	auto root_children = root->get_children();

	ASSERT_EQ(root_children.size(), 2);

	auto doctype = root_children[0];

	ASSERT_TRUE(doctype->get_children().empty());
	ASSERT_TRUE(doctype->get_content().empty());
	ASSERT_EQ(doctype->get_name(), "!DOCTYPE");
	ASSERT_EQ(doctype->get_type(), DocumentObjectType::SpecialTag);
	ASSERT_EQ(doctype->get_parent(), root);

	auto doctype_attributes = doctype->get_attributes();

	ASSERT_EQ(doctype_attributes.size(), 1);

	auto doctype_attribute = doctype_attributes[0];

	ASSERT_EQ(doctype_attribute.name, "html");
	ASSERT_EQ(doctype_attribute.value, "");

	auto html = root_children[1];

	ASSERT_TRUE(html->get_content().empty());
	ASSERT_TRUE(html->get_attributes().empty());
	ASSERT_EQ(html->get_type(), DocumentObjectType::Tag);
	ASSERT_EQ(html->get_parent(), root);

	auto html_children = html->get_children();

	ASSERT_EQ(html_children.size(), 1);

	auto body = html_children[0];

	ASSERT_TRUE(body->get_content().empty());
	ASSERT_TRUE(body->get_attributes().empty());
	ASSERT_EQ(body->get_type(), DocumentObjectType::Tag);
	ASSERT_EQ(body->get_parent(), html);

	auto body_children = body->get_children();

	ASSERT_EQ(body_children.size(), 3);

	auto h1 = body_children[0];

	ASSERT_EQ(h1->get_name(), "h1");
	ASSERT_EQ(h1->get_type(), DocumentObjectType::Tag);
	ASSERT_TRUE(h1->get_children().empty());
	ASSERT_TRUE(h1->get_attributes().empty());
	ASSERT_EQ(h1->get_parent(), body);
	ASSERT_EQ(h1->get_content(), "My First Heading");

	auto comment = body_children[1];

	ASSERT_EQ(comment->get_type(), DocumentObjectType::Comment);
	ASSERT_TRUE(comment->get_children().empty());
	ASSERT_TRUE(comment->get_attributes().empty());
	ASSERT_EQ(comment->get_parent(), body);
	ASSERT_EQ(comment->get_content(), "Comment");

	auto p = body_children[2];

	ASSERT_EQ(p->get_name(), "p");
	ASSERT_EQ(p->get_type(), DocumentObjectType::Tag);
	ASSERT_TRUE(p->get_children().empty());
	ASSERT_TRUE(p->get_attributes().empty());
	ASSERT_EQ(p->get_parent(), body);
	ASSERT_EQ(p->get_content(), "My first paragraph.");
}

TEST(HtmlParserTest, JiraPageTest)
{
	domParser::HtmlParser parser;
	auto root = domParser::SharedDocumentObject();
	
	root = parser.get_dom_object("jira-test.html");
	root = parser.get_dom_object("cpp_npos_page.html");
}

int main(int argc, char** argv)
{
	testing::InitGoogleTest(&argc, argv);
	RUN_ALL_TESTS();
	_getwch();
}