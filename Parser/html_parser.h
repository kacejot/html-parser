#pragma once
#include <fstream>
#include <stack>
#include <map>

#include "document_object.h"
#include "abstract_parser.h"
#include "state.h"

namespace domParser
{
	class HtmlParser : private IStateParser
	{
	public:
		HtmlParser();
		virtual ~HtmlParser() override;
		SharedDocumentObject get_dom_object(const std::string& path);

	private:
		virtual SharedDocumentObject get_stack_top_element() override;
		virtual void push_to_stack_empty_object() override;
		virtual void set_state(StateType type) override;
		virtual void unwind_stack() override;

		SharedDocumentObject finish_parsing();
		void handle_char(const unsigned char c);

	private:
		SharedParserState current_state;
		std::map<StateType, SharedParserState> state_map;
		std::stack<SharedDocumentObject> tag_stack;
	};
}