#include "html_parser.h"
#include <stack>

namespace domParser
{
	HtmlParser::HtmlParser() : tag_stack()
	{
		current_state = std::make_unique<UndefinedState>();

		state_map = std::map<StateType, SharedParserState>
		{
			{ StateType::SeekForTag, std::make_shared<SeekForTag>() },
			{ StateType::ReadingName, std::make_shared<ReadingName>() },
			{ StateType::ReadingAttributes, std::make_shared<ReadingAttributes>() },
			{ StateType::ReadingContent, std::make_shared<ReadingContent>() },
			{ StateType::ReadingClosingName, std::make_shared<ReadingClosingName>() },
			{ StateType::PossibleSpecialTag, std::make_shared<PossibleSpecialTag>() },
			{ StateType::ReadingComment, std::make_shared<ReadingComment>() },
			{ StateType::UndefinedState, std::make_shared<UndefinedState>() }
		};
	}

	HtmlParser::~HtmlParser()
	{

	}

	SharedDocumentObject HtmlParser::get_dom_object(const std::string& path)
	{	
		// Set default values to the parser
		tag_stack = std::stack<SharedDocumentObject>();
		current_state = std::make_unique<SeekForTag>();

		// Create root tree element, that contains file info
		tag_stack.push(std::make_shared<DocumentObject>(path, DocumentObjectType::File));


		unsigned char debug_char = '\0';


		std::ifstream input_file(path);
		while (input_file.good()) 
		{
			debug_char = static_cast<unsigned char>(input_file.get());
			handle_char(debug_char);
		}

		if (input_file.eof())
		{
			return finish_parsing();
		}

		if (input_file.fail())
		{
			throw std::runtime_error("Logical or read/writing error on i/o operation.");
		}

		return nullptr;
	}

	SharedDocumentObject HtmlParser::get_stack_top_element()
	{
		return tag_stack.top();
	}

	SharedDocumentObject HtmlParser::finish_parsing()
	{
		ObjectList children_list;
		auto current = tag_stack.top();

		while (current->get_type() != DocumentObjectType::File)
		{
			children_list.push_front(current);
			tag_stack.pop();
			current = tag_stack.top();
		}

		if (tag_stack.size() == 1)
		{
			DocumentObjects children
			{
				std::make_move_iterator(std::begin(children_list)),
				std::make_move_iterator(std::end(children_list))
			};

			for each (auto child in children)
			{
				child->set_parent(current);
			}

			current->set_children(children);
			return current;
		}
		else
		{
			int size = tag_stack.size();
			throw std::runtime_error("Unexpected end of parsing. Stack contains " + tag_stack.size());
		}

		return nullptr;
	}

	void HtmlParser::unwind_stack()
	{	
		std::string closing_name = tag_stack.top()->get_name();
		tag_stack.pop();

		ObjectList children_list;
		SharedDocumentObject current = tag_stack.top();

		while (true)
		{
			if (current->get_name() == closing_name && !current->is_closed())
			{
				DocumentObjects children 
				{
					std::make_move_iterator(std::begin(children_list)),
					std::make_move_iterator(std::end(children_list))
				};

				for each (auto child in children)
				{
					child->set_parent(current);
				}

				current->set_children(children);
				current->close();
				return;
			}

			if (current->get_type() == DocumentObjectType::File || tag_stack.empty())
			{
				throw std::runtime_error("Didn't find start of the tag while was unwinding.");
			}

			children_list.push_front(current);
			tag_stack.pop();
			current = tag_stack.top();
		}
	}

	void HtmlParser::set_state(StateType type)
	{
		current_state->clear();
		current_state = state_map.at(type);
	}

	void HtmlParser::push_to_stack_empty_object()
	{
		tag_stack.push(std::make_shared<DocumentObject>());
	}

	void HtmlParser::handle_char(const unsigned char c)
	{
		current_state->handle(*this, c);
	}
}