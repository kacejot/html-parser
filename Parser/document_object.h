#pragma once
#include <memory>
#include <vector>
#include <list>
#include <map>
#include <stack>

#include "attribute.h"

namespace domParser
{
	class DocumentObject;

	// DocumentObject pointers
	using SharedDocumentObject = std::shared_ptr<DocumentObject>;
	using WeakDocumentObject = std::weak_ptr<DocumentObject>;

	// DocumentObject containers
	using DocumentObjects = std::vector<SharedDocumentObject>;
	using ObjectStack = std::stack<SharedDocumentObject>;
	using ObjectList = std::list<SharedDocumentObject>;

	enum class DocumentObjectType 
	{
		Undefined,
		File,
		Tag,
		SpecialTag,
		Comment
	};

	class DocumentObject
	{
	public:
		DocumentObject();
		DocumentObject(const std::string& name, DocumentObjectType type, SharedDocumentObject parent = nullptr);

		void set_name(const std::string& name);
		std::string get_name() const;

		SharedDocumentObject get_parent();
		void set_parent(SharedDocumentObject parent);

		std::string get_content();
		void set_content(const std::string& content);

		DocumentObjects& get_children();
		void add_child(SharedDocumentObject child);
		void set_children(DocumentObjects& children);

		Attributes& get_attributes();
		void set_attributes(Attributes& attrs);
		void add_attribute(const Attribute& attr);

		DocumentObjectType get_type();
		void set_type(DocumentObjectType type);

		void write_to_html(const std::string& path);

		bool is_closed();
		void close();

	private:
		bool closed_;
		std::string name_;
		std::string content_;
		Attributes attrs_;
		WeakDocumentObject parent_;
		DocumentObjects children_;
		DocumentObjectType type_;
	};
}

