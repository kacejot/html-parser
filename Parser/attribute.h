#pragma once
#include <vector>

namespace domParser
{
	struct Attribute;
	using Attributes = std::vector<Attribute>;

	struct Attribute
	{
		Attribute(const std::string& name, const std::string& value);

		std::string name;
		std::string value;
	};
}