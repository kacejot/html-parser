#pragma once
#include <memory>
#include "document_object.h"

namespace domParser
{
	class IParserState;
	using SharedParserState = std::shared_ptr<IParserState>;

	enum class StateType;

	class IStateParser
	{
	public:
		virtual SharedDocumentObject get_stack_top_element() = 0;
		virtual void set_state(StateType type) = 0;
		virtual void push_to_stack_empty_object() = 0;
		virtual void unwind_stack() = 0;
		virtual ~IStateParser() { }
	};
}

