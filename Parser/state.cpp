#include "state.h"

namespace domParser
{
	bool is_delimiter(char context)
	{
		switch (context)
		{
		case ' ':
		case '\n':
		case '\r':
		case '\t':
			return true;
		default:
			return false;
		}
	}

	SeekForTag::~SeekForTag()
	{

	}

	void SeekForTag::handle(IStateParser& parser, const unsigned char context)
	{
		if (is_delimiter(context))
		{
			return;
		}

		if (LEFT_ANGLE_BRACKET == context)
		{
			parser.push_to_stack_empty_object();
			parser.set_state(StateType::ReadingName);
			return;
		}
	}

	void SeekForTag::clear()
	{

	}

	ReadingName::ReadingName() : name("")
	{

	}
	
	ReadingName::~ReadingName()
	{

	}

	void ReadingName::handle(IStateParser& parser, const unsigned char context)
	{
		if (name.empty())
		{
			if (is_delimiter(context))
			{
				return;
			}

			if (EXCLAMATION_MARK == context)
			{
				parser.get_stack_top_element()->set_type(DocumentObjectType::SpecialTag);
				parser.set_state(StateType::PossibleSpecialTag);
				return;
			}
		}

		if (is_delimiter(context))
		{
			parser.get_stack_top_element()->set_name(name);
			parser.set_state(StateType::ReadingAttributes);
			return;
		}

		if (SLASH == context)
		{
			parser.set_state(StateType::ReadingClosingName);
			return;
		}

		if (RIGHT_ANGLE_BRACKET == context)
		{
			parser.get_stack_top_element()->set_name(name);
			parser.set_state(StateType::ReadingContent);
			return;
		}
			
		name.push_back(context);
	}

	void ReadingName::clear()
	{
		name = "";
	}

	ReadingAttributes::ReadingAttributes() : reading_value(false), reading_quotes(false), attrs(), current_attribute_name(""), current_attribute_value("")
	{
		
	}

	ReadingAttributes::~ReadingAttributes()
	{

	}

	void ReadingAttributes::handle(IStateParser& parser, const unsigned char context)
	{
		if (is_delimiter(context) && !reading_quotes)
		{
			if (current_attribute_name.empty())
			{
				return;
			}
			else
			{
				attrs.push_back(Attribute(current_attribute_name, current_attribute_value));
				current_attribute_name = "";
				current_attribute_value = "";
				reading_value = false;
				return;
			}
		}

		if (QUOTES == context && reading_value)
		{
			if (reading_quotes)
			{
				reading_quotes = false;
			}
			else
			{
				reading_quotes = true;
			}

			current_attribute_value.push_back(context);
			return;
		}

		if (EQUALS == context)
		{
			reading_value = true;
			return;
		}

		if (RIGHT_ANGLE_BRACKET == context)
		{
			if (!current_attribute_name.empty())
			{
				attrs.push_back(Attribute(current_attribute_name, current_attribute_value));
				current_attribute_name = "";
				current_attribute_value = "";
			}

			if (!attrs.empty())
			{
				parser.get_stack_top_element()->set_attributes(attrs);
			}
			
			parser.set_state(StateType::ReadingContent);
			return;
		}

		if (reading_value)
		{
			current_attribute_value.push_back(context);
		}
		else
		{
			current_attribute_name.push_back(context);
		}
	}

	void ReadingAttributes::clear()
	{
		reading_value = false;
		reading_quotes = false;
		attrs = Attributes();
		current_attribute_name = "";
		current_attribute_value = "";
	}


	ReadingContent::ReadingContent() : only_delimiters(true),tag_content("")
	{

	}

	ReadingContent::~ReadingContent()
	{

	}

	void ReadingContent::handle(IStateParser& parser, const unsigned char context)
	{
		if (LEFT_ANGLE_BRACKET == context && only_delimiters)
		{
			parser.push_to_stack_empty_object();
			parser.set_state(StateType::ReadingName);
			return;
		}

		if (!is_delimiter(context))
		{
			only_delimiters = false;
		}

		tag_content.push_back(context);

		std::string closing_condition = "</" + parser.get_stack_top_element()->get_name() + ">";
		std::size_t pos = tag_content.find(closing_condition);

		if (pos != std::string::npos)
		{
			std::string content = tag_content.substr(0, pos);
			parser.get_stack_top_element()->set_content(content);
			parser.set_state(StateType::SeekForTag);
		}
	}

	void ReadingContent::clear() 
	{
		only_delimiters = true;
		tag_content = "";
	}

	ReadingClosingName::ReadingClosingName() : name("")
	{

	}

	ReadingClosingName::~ReadingClosingName()
	{

	}

	void ReadingClosingName::handle(IStateParser& parser, const unsigned char context)
	{
		if (RIGHT_ANGLE_BRACKET == context)
		{
			parser.get_stack_top_element()->set_name(name);
			parser.unwind_stack();
			parser.set_state(StateType::SeekForTag);
			return;
		}

		name.push_back(context);
	}

	void ReadingClosingName::clear() 
	{
		name = "";
	}

	PossibleSpecialTag::PossibleSpecialTag() : name(""), counter(0)
	{

	}

	PossibleSpecialTag::~PossibleSpecialTag()
	{

	}

	void PossibleSpecialTag::handle(IStateParser& parser, const unsigned char context)
	{
		if (name.empty())
		{
			if (DASH == context)
			{
				++counter;

				if (counter == 2)
				{
					parser.get_stack_top_element()->set_type(DocumentObjectType::Comment);
					parser.set_state(StateType::ReadingComment);
					return;
				}

				return;
			}
		}
		
		if (is_delimiter(context))
		{
			parser.get_stack_top_element()->set_name('!' + name);
			parser.set_state(StateType::ReadingAttributes);
			return;
		}

		if (RIGHT_ANGLE_BRACKET == context)
		{
			parser.set_state(StateType::ReadingContent);
			return;
		}

		name.push_back(context);
	}

	void PossibleSpecialTag::clear() 
	{
		name = "";
		counter = 0;
	}

	ReadingComment::ReadingComment() : comment_content(""), counter(0)
	{
	
	}

	ReadingComment::~ReadingComment()
	{

	}

	void ReadingComment::handle(IStateParser& parser, const unsigned char context)
	{
		if (DASH == context)
		{
			++counter;
		}
		else
		{
			if (counter == 2 && RIGHT_ANGLE_BRACKET == context)
			{
				std::string content = comment_content.substr(0, comment_content.size() - counter);
				parser.get_stack_top_element()->set_content(content);
				parser.set_state(StateType::SeekForTag);
				return;
			}
			else
			{
				counter = 0;
			}
		}

		comment_content.push_back(context);
	}

	void ReadingComment::clear() 
	{
		comment_content = "";
		counter = 0;
	}

	UndefinedState::~UndefinedState()
	{

	}

	void UndefinedState::handle(IStateParser& parser, const unsigned char context)
	{
		throw std::runtime_error("Undefined parser satate. You shouldn't be here.");
	}

	void UndefinedState::clear()
	{
	
	}
}