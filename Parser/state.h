#pragma once
#include <string>
#include "document_object.h"
#include "abstract_parser.h"
#include "constants.h"

namespace domParser
{	
	enum class StateType
	{
		SeekForTag,
		ReadingName,
		ReadingAttributes,
		ReadingContent,
		ReadingClosingName,
		PossibleSpecialTag,
		ReadingComment,
		UndefinedState
	};

	class IParserState
	{
	public:
		virtual ~IParserState() { }
		virtual void handle(IStateParser& parser, const unsigned char context) = 0;
		virtual void clear() = 0;
	};

	class SeekForTag : public IParserState
	{
	public:
		virtual ~SeekForTag() override;
		virtual void handle(IStateParser& parser, const unsigned char context) override;
		virtual void clear() override;
	};

	class ReadingName : public IParserState
	{
	public:
		ReadingName();
		virtual ~ReadingName() override;
		virtual void handle(IStateParser& parser, const unsigned char context) override;
		virtual void clear() override;
	private:
		std::string name;
	};

	class ReadingAttributes : public IParserState
	{
	public:
		ReadingAttributes();
		virtual ~ReadingAttributes() override;
		virtual void handle(IStateParser& parser, const unsigned char context) override;
		virtual void clear() override;

	private:
		bool reading_value;
		bool reading_quotes;
		std::string current_attribute_name;
		std::string current_attribute_value;
		Attributes attrs;
	};

	class ReadingContent : public IParserState
	{
	public:
		ReadingContent();
		virtual ~ReadingContent() override;
		virtual void handle(IStateParser& parser, const unsigned char context) override;
		virtual void clear() override;

	private:
		bool only_delimiters;
		std::string tag_content;
	};

	class ReadingClosingName : public IParserState
	{
	public:
		ReadingClosingName();
		virtual ~ReadingClosingName() override;
		virtual void handle(IStateParser& parser, const unsigned char context) override;
		virtual void clear() override;

	private:
		std::string name;
	};

	class PossibleSpecialTag : public IParserState
	{
	public:
		PossibleSpecialTag();
		virtual ~PossibleSpecialTag() override;
		virtual void handle(IStateParser& parser, const unsigned char context) override;
		virtual void clear() override;
	private:
		std::string name;
		int counter;
	};

	class ReadingComment : public IParserState
	{
	public:
		ReadingComment();
		virtual ~ReadingComment() override;
		virtual void handle(IStateParser& parser, const unsigned char context) override;
		virtual void clear() override;
	private:
		std::string comment_content;
		int counter;
	};

	class UndefinedState : public IParserState
	{
	public:
		virtual ~UndefinedState() override;
		virtual void handle(IStateParser& parser, const unsigned char context) override;
		virtual void clear() override;
	};
}
