#pragma once

namespace domParser
{
	const unsigned char RIGHT_ANGLE_BRACKET = '>';
	const unsigned char LEFT_ANGLE_BRACKET = '<';
	const unsigned char EXCLAMATION_MARK = '!';
	const unsigned char EQUALS = '=';
	const unsigned char SLASH = '/';
	const unsigned char DASH = '-';
	const unsigned char QUOTES = '\"';
}