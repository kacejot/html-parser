#include "document_object.h"

namespace domParser
{
	DocumentObject::DocumentObject() : closed_ (false), attrs_(), parent_(), name_(""), content_(""), type_(DocumentObjectType::Tag)
	{

	}

	DocumentObject::DocumentObject(const std::string& name, DocumentObjectType type, SharedDocumentObject parent) : closed_(false), name_(name), content_(), parent_(parent), type_(type)
	{

	}

	void DocumentObject::set_name(const std::string& new_name)
	{
		this->name_ = new_name;
	}

	std::string DocumentObject::get_name() const
	{
		return this->name_;
	}

	std::string DocumentObject::get_content()
	{
		return this->content_;
	}

	void DocumentObject::set_content(const std::string& content)
	{
		this->content_ = content;
	}

	SharedDocumentObject DocumentObject::get_parent()
	{
		return parent_.lock();
	}

	void DocumentObject::set_parent(SharedDocumentObject parent)
	{
		this->parent_ = parent;
	}

	DocumentObjects& DocumentObject::get_children()
	{
		return children_;
	}

	void DocumentObject::add_child(SharedDocumentObject child)
	{
		this->children_.push_back(child);
	}

	void DocumentObject::set_children(DocumentObjects& children)
	{
		this->children_ = std::move(children);
	}

	Attributes& DocumentObject::get_attributes()
	{
		return this->attrs_;
	}

	void DocumentObject::set_attributes(Attributes& attrs)
	{
		this->attrs_ = std::move(attrs);
	}

	void DocumentObject::add_attribute(const Attribute& attr)
	{
		this->attrs_.push_back(attr);
	}

	DocumentObjectType DocumentObject::get_type()
	{
		return type_;
	}

	void DocumentObject::set_type(DocumentObjectType type)
	{
		type_ = type;
	}

	void DocumentObject::write_to_html(const std::string& path)
	{
		//TODO: specify writing behavior for tag and comment separately
		for (auto child : this->get_children())
		{
			child->write_to_html(path);
		}
	}

	bool DocumentObject::is_closed()
	{
		return closed_;
	}

	void DocumentObject::close()
	{
		closed_ = true;
	}
}